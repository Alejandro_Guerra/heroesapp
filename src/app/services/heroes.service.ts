import { Injectable } from '@angular/core';
import { Http , Headers } from '@angular/http';
import { Heroe } from '../interfaces/interface.heroe';
import 'rxjs/Rx';

@Injectable()
export class HeroesService {

  newFireUrl:string ="https://heroesapp-dd7eb.firebaseio.com/heroes.json"
  getAndUpdateFireUrl:string ="https://heroesapp-dd7eb.firebaseio.com/heroes/"


  constructor(private http:Http) {

  }

  nuevoHeroe(heroe:Heroe){
    let body = JSON.stringify(heroe);
    let headers = new Headers({
      'Content-Type':'application/json'
    });

    return this.http.post( this.newFireUrl, body, { headers } )
      .map( res=>{
          console.log(res.json());
          return res.json();
      })
  }

  actualizarHeroe(heroe:Heroe, key$:string){
    let body = JSON.stringify(heroe);
    let headers = new Headers({
      'Content-Type':'application/json'
    });

    let url = `${this.getAndUpdateFireUrl}/${key$}.json`;

    return this.http.put( url, body, { headers } )
      .map( res=>{
          console.log(res.json());
          return res.json();
      })
  }

  borrarHeroe(key$:string){
    let  url = `${this.getAndUpdateFireUrl}/${key$}.json`;
    return this.http.delete(url)
      .map(res => res.json());
  }

  getHeroe(key$:string){
    let url = `${this.getAndUpdateFireUrl}/${key$}.json`;
    return this.http.get(url)
      .map(res => res.json());
  }

  getHeroes(){
    let url = `${this.getAndUpdateFireUrl}/.json`;
    return this.http.get(url)
      .map(res => res.json());
  }

}
